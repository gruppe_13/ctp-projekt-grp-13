## Table of contents

[[_TOC_]]

## Introduction

------------

This is our first project. 
The authors: Artin Ghalamkary, Karsten Bak Malle, Phillip Ravn Boe Jensen.

\________________________________________________________________________________________________________

“Will robots inherit the earth? Yes, but they will be our children” - Marvin Minsky

The motive of this project is to acquire hands-on experience with essential technical parameters such as programming, designing, implementing and testing autonomous navigation software for a given robot. In doing so, a plausible understanding of ROS, an operating system for robots, and Gazebo, a 3D robotics simulator, has been gained. The overall project has spanned over 14 weeks, with time included inorder to fully improve, optimize, and complete the maze solving algorithm, as well as to write a report concerning the journey.


Before diving into the different parameters, mentality and goal of how the robot ought to maneuver, it's essential to have a background in the understanding of:
    
    1. How ROS ties into the whole system of working together with certain parameters of the robot,
       such that certain aspects of the robot are accessible, interchangeable, and optimizable. 
       Compactly described, ROS has key features that allow for communication and exchange of 
       information between the actual robot and the code.

    2. The general idea for the robots navigation. Many ideologies are plausible, however the path 
       chosen was the left-wall hugging algorithm, which by name is self-explanatory, but which 
       also will be described directly below. 


## Rapport: 

https://imgur.com/a/zkI906h 


## Requirements

ROS noetic (http://wiki.ros.org/noetic), 
gazebo (http://gazebosim.org/),
Python3 (https://www.python.org/downloads/)

## Installation

1. Clone the git repository to your local device: 
```
git clone https://gitlab.com/gruppe_13/ctp-projekt-grp-13.git
```
2. Open the "CTP projekt - grp 13" folder and drag the contents into /home/<your-username>/

3. Now open the terminal in /home/<your-username>/ and type:
```
cd catkin_ws
```
```
catkin_make
```
```
roslaunch turtlebot3_gazebo turtlebot3_maze2.launch
```
4. Open new terminal and type:
```
rosrun turtlebot3_gazebo mazesolve_bruh.py
```

## Extra

Experience our faster but more volatile maze traversal.

Instead of step 4 in the installation section, use the following command in the terminal:

```
rosrun turtlebot3_gazebo mazesolve_bruh_fast.py
```

This was made specificly for "turtlebot3_maze2.launch" and is NOT recommended for different mazes.

## Configuration

Make sure to have the latest updates of the programs from the requirements section.


## Troubleshooting & FAQ

For assistance, email us at: 202006348@post.au.dk

