#!/usr/bin/env python3

#  Copyright (c) <2021> <Mikkel Heinrich, Aleksander Nikolajsen, Tobias Frejo Rasmussen>

#  Permission is hereby granted, free of charge, to any person 
#  obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use,
#  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy
import numpy as np

# Global variables
CENTER  = 0
LEFT    = 1
RIGHT   = 2

GET_DIRECTION    = 0
DRIVE_FORWARD    = 1
RIGHT_TURN       = 2
LEFT_TURN        = 3
RIGHT_TURN_STILL = 4
LEFT_TURN_STILL  = 5

LINEAR_VELOCITY = 0.3
ANGULAR_VELOCITY = 1.0

class WalleDrive():
    # Class variables - non-initialized
    walle_state_num = 0                     # Used to decide direction.

    cmd_vel_pub_:rospy.Publisher = None     # Publisher     - motion.
    laser_scan_sub_:rospy.Subscriber = None # Subscriber    - angles (for collision detection).
    odom_sub_:rospy.Subscriber = None       # Subscriber    - ???

    check_forward_dist_ = 0.0               # Break distance.
    check_side_dist_ = 0.0                  # Distance from wall (don't hug the wall!).
    dist_tolerance = 0.0                    # Error... Unused!

    scan_data_ = [0.0, 0.0, 0.0]            # Normal scan angles (front, left, right).
    big_scan_data_ = [0.0] * 181            # Angle of 45 degree on left and on right (90 total, with 1 as the 0th degree).

    walle_pose_ = 0.0                       # current position of robot.
    prev_walle_pose_ = 0.0                  # Previous position of robot.

    is_colided = False
    collisions = 0
    speed_updates = 0
    speed_accumulated = 0.0
    speed_average = 0.0

    path_width = 0.0
    free_path_theta_min:int = 0
    free_path_theta_max:int = 0

    current_lin_vel = 0
    current_pos = (0, 0)

    #Constructor
    def __init__(self):

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        # initialize variables
        self.check_forward_dist_ = 0.5
        self.check_side_dist_ = 0.65
        self.dist_tolerance = 0.1

        self.walle_pose_ = 0.0
        self.prev_walle_pose_ = 0.0

        self.path_width = 0.25
        self.free_path_theta_min = 0
        self.free_path_theta_max = int(np.ceil(np.arctan(self.check_forward_dist_ / self.path_width) * (180/np.pi)))

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallBack, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)

    # Destructor
    def __del__(self):
        pass

    # Odometry
    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)

        self.walle_pose_ = np.arctan2(siny, cosy)
        self.current_pos = (data.pose.pose.position.x, data.pose.pose.position.y)

    #Utilizing the laserscanner
    def laserScanMsgCallBack(self, data:LaserScan):
        scan_angle = [0, 30, 330]                               # Normal scan angles (front, left, right)

        # Data collection for scan_data (normal can angles)
        for i in range(len(scan_angle)):
            if np.isinf(data.ranges[i]):
                self.scan_data_[i] = data.range_max
            else:
                self.scan_data_[i] = data.ranges[scan_angle[i]]
        
        # Data collection for big_scan_data (wide-scale area)
        for i in range(181):
            # map range [0;180] to range [90;-90]
            angle = 90-i
            if np.isinf(data.ranges[angle]):
                self.big_scan_data_[i] = data.range_max
            else:
                self.big_scan_data_[i] = data.ranges[angle]

        if self.scan_data_[CENTER] < 0.1 or min(self.scan_data_[1:]) < 0.14:
            if not self.is_colided:
                rospy.logwarn("Wall-E has collided")
                self.collisions += 1
                self.is_colided = True
        elif self.is_colided:
            self.is_colided = False
            rospy.logwarn("Wall-E is free!")

    # For updating both the linear and the angular velocity
    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()                   # controls motion of robot.
        cmd_vel.linear.x = lin_vel          # Linear speed.
        cmd_vel.angular.z = ang_vel         # Angular speed.

        self.speed_accumulated += lin_vel
        self.speed_updates += 1
        self.speed_average = self.speed_average + (lin_vel-self.speed_average)/self.speed_updates   # Won't overflow

        self.current_lin_vel = lin_vel

        #rospy.loginfo(cmd_vel)              # Log data of robot position and speeds.
        self.cmd_vel_pub_.publish(cmd_vel)  # Print logged data in terminal.

    # For motion (left, right, front, dead end avoidance) - direction variable is currenty not being used as intended.
    def turn(self, pct: float, direction = 1):
        lin_pct = np.abs(1.0 - np.abs(pct))                                                         # 100% - Wished percentage - ERROR! (arrow down).
        self.updatecommandVelocity(LINEAR_VELOCITY * direction * lin_pct, ANGULAR_VELOCITY * pct)   # Update of velocity and direction
    
    def free_path(self):
        """Check if theres a clear path right ahead.
        
        returns: 
         0: Path is free
         1: Obstacle to the left
        -1: Obstacle to the right
        """

        for i in range(self.free_path_theta_min, self.free_path_theta_max):
            

            if self.big_scan_data_[180-i] < self.path_width/np.cos(i*(np.pi/180)):  # If length of laser, from right to left, is less than the length of the hypotenus then detect obstacle to the right
                return -1
            elif self.big_scan_data_[i] < -self.path_width/np.cos(i*(np.pi/180)):   # The same applies here but mirrored
                return 1
        return 0
    
    def corner_turn_angle(self, side):
        #rospy.loginfo("Corner turn angle")
        turn_pct = 0.0
        for i in range(91):
            theta = 90-i
            if side == RIGHT:
                idx = 180-i         # idx = 180 is angle -90, and idx = 90 is angle 0
            elif side == LEFT:
                idx = i
            else:
                break
            
            if self.big_scan_data_[idx] > self.check_side_dist_:
                if theta < 30:
                    turn_pct = 0.0
                elif theta > 90:
                    turn_pct = 1.0
                else:
                    turn_pct = theta/60 - 0.5 # at 90deg turn 100%, at 30deg turn 0
                break

        if side == RIGHT:
            return -turn_pct
        elif side == LEFT:
            return turn_pct
        else:
            rospy.logerr(f"corner_turn_angle: Wrong side {side}")
            return None

    # Control loop - motion in cohesion with sensors
    def controlLoop(self):
        rate = rospy.Rate(125) # 125 Hz - used for delay in end of code...

        # Locate the angle with the longest distance to an object, within big_scan_data-area
        # Angle is found by 90 degrees minus i resulting in scan-range: [45:136]
        biggest_angle = self.big_scan_data_[45:136].index(max(self.big_scan_data_[45:136])) # [-45;45]
        fwd_turn_pct = -(biggest_angle-45)/45 # [-1.0; 1.0]

        # Collision detection
        if self.walle_state_num == GET_DIRECTION:                       # if the direction is not known, then check data.
            #rospy.loginfo("Get Direction")
            #rospy.loginfo(f"scan_data: {self.scan_data_}")

            ## no frontal obstacles detected
            if self.scan_data_[CENTER] > self.check_forward_dist_:      ## if the distance in front of Wall-E is greater than check_forward then,
                ### check side collision.
                if self.scan_data_[RIGHT] > self.check_side_dist_:       ### If there's space on the right, turn right.
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = RIGHT_TURN

                elif self.scan_data_[LEFT] > self.check_side_dist_:    ### else if there's space to the left, turn left.
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = LEFT_TURN

                else:                                                   ### else drive forward.
                    self.walle_state_num = DRIVE_FORWARD

            ## frontal obstacles detected
            else:                                                       ## if the distance in front of Wall-E is less than check_forward then,
                self.prev_walle_pose_ = self.walle_pose_

                if max(self.big_scan_data_) < self.check_forward_dist_: ### Dead end entered
                    #rospy.logwarn("Dead End Detected")
                    self.walle_state_num = RIGHT_TURN_STILL
                
                else:
                    biggest_angle = self.big_scan_data_.index(max(self.big_scan_data_)) # [-90;90]
                    if biggest_angle > 90:
                        self.walle_state_num = RIGHT_TURN_STILL
                    else:
                        self.walle_state_num = LEFT_TURN_STILL

        # Drive forward
        elif self.walle_state_num == DRIVE_FORWARD:
            #rospy.loginfo("Forward Drive")
            if self.free_path() == 0:
                self.turn(fwd_turn_pct*0.5)
            else:
                self.turn(0)
            self.walle_state_num = GET_DIRECTION
        
        # Turn Right
        elif self.walle_state_num == RIGHT_TURN:
            #turn_pct = 1.0 - np.min([(self.scan_data_[LEFT]/self.check_side_dist_), 1.0])
            turn_pct = self.corner_turn_angle(RIGHT)
            #rospy.loginfo(f"Right Turn, {np.round(turn_pct, 5)}")
            self.turn(turn_pct)
            self.walle_state_num = GET_DIRECTION
        
        # Turn Left
        elif self.walle_state_num == LEFT_TURN:
            #turn_pct = 1.0 - np.min([(self.scan_data_[RIGHT]/self.check_side_dist_), 1.0])
            turn_pct = self.corner_turn_angle(LEFT)
            #rospy.loginfo(f"Left Turn, {np.round(turn_pct, 5)}")
            self.turn(turn_pct)
            self.walle_state_num = GET_DIRECTION

        elif self.walle_state_num == RIGHT_TURN_STILL:
            #rospy.loginfo("Right Still Turn")
            self.turn(-1.0)
            self.walle_state_num = GET_DIRECTION

        elif self.walle_state_num == LEFT_TURN_STILL:
            #rospy.loginfo("Left Still Turn")
            self.turn(1.0)
            self.walle_state_num = GET_DIRECTION

        else:                                                                           # else rerun collision-detection.
            self.walle_state_num = GET_DIRECTION

        rate.sleep()                                                                    # Delay before next action.

# Main
if __name__ == "__main__":
    rospy.init_node("walle_pydrive")

    bot = WalleDrive()

    while not rospy.is_shutdown():                  # As long as the script is running, then run controlLoop().
        try:
            bot.controlLoop()

            data = {
                'current_lin_vel': bot.current_lin_vel,
                'lin_avg': bot.speed_average,
                'collisions': bot.collisions,
                'pos' : {
                    'x': bot.current_pos[0],
                    'y': bot.current_pos[1]
                }
            }
            rospy.loginfo(data)

        except:
            rospy.loginfo(f"Average linear velocity: {bot.speed_average}")
            rospy.loginfo(f"collisions: {bot.collisions}")
            exit()