# How to use

1. Make sure your program outputs to the terminal in a format like this
```
[INFO] [1620219145.478304, 249.932000]: {'current_lin_vel': 0, 'lin_avg': 0.0, 'collisions': 0, 'pos': {'x': 0, 'y': 0}}
```
2. Pipe the output to a file. e.g. with 
```
rosrun package program > logs/output.txt
```
or 
```
rosrun package program | tee logs/output.txt
```
3. Use `data_parse.py` to convert the output to json. (I know it is not optimal and a csv would be better)
```
python3 data_parse.py logs/output.txt
```
4. Open the jupyter notebook `data_processing.ipynb` and make the plots.