#%% 
import json
import numpy as np
import argparse
import os
import re

#%%
def parse_line(line):
    severity_regex = "\[[A-Z]*\]"
    time_re = re.search("\[(\d*)\.\d*, \d*\.\d*\]", line).group()
    time = re.findall("\d*\.\d*", time_re)[1]
    data_str = re.search("\{.*\}", line).group()
    data_str = data_str.replace("'", '"')
    data = json.loads(data_str)
    data['time'] = float(time)
    return data

def log2json(filename, outname=None):
    if not outname:
        outname = f'{os.path.splitext(filename)[0]}.json'
    
    logfile = open(filename, 'r')
    lines = logfile.readlines()

    data = []

    for line in lines:
        d = parse_line(line)
        data.append(d)
    
    with open(outname, 'w') as outfile:
        json.dump(data, outfile)
    

#%%
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=str, action='store')
    args = parser.parse_args()

    log2json(args.file)
