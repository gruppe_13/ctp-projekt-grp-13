import rospy, time
import numpy as np
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

range_front = []
range_right = []
range_left  = []
min_front = 0
i_front = 0
min_right = 0
i_right = 0
min_left = 0
i_left = 0

near_wall = 0 # start with 0, when we get to a wall, change to 1

class Robot():

    # Initialize all variables

    cmd_vel_pub:rospy.Publisher = None #publisher
    scan_sub:rospy.Subscriber = None #subscriber

    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0

    collisions = 0.0
    update_num = 0.0
    avg_speed = 0.0
    sum_speed = 0.0
    current_pose = 0.0

    def __init__(self):
        # Create the node
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size = 1) # to move the robot
        self.scan_sub = rospy.Subscriber('scan', LaserScan, self.scan_callback)   # to read the laser scanner
        #rospy.init_node('maze_explorer')

        #self.command = Twist() #delete???
        #self.command.linear.x = 0.0
        #self.command.angular.z = 0.0
        
        #self.rate = rospy.Rate(10)
        #time.sleep(1) # wait for node to initialize

        self.position = Odometry() #for printing
        self.current_position = (self.position.pose.pose.position.x, self.position.pose.pose.position.y)

        #dont need odomMessage... ?

        #def odomMessageCallback(self, data:Odometry):
                    #siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
                    #cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)
                    
                    #self.pose = np.arctan2(siny, cosy)

    #def Lin_Speed(self, 

    def __del__(self):
        pass

    def scan_callback(self, msg):
        global range_front
        global range_right
        global range_left
        global ranges
        global min_front,i_front, min_right,i_right, min_left ,i_left
            
        # Scan  for differented ranges of angles at the fron right and left
        ranges = msg.ranges
        # in front of the robot (between 5 to -5 degrees)
        range_front[:5] = msg.ranges[5:0:-1]  
        range_front[5:] = msg.ranges[-1:-5:-1]
        # to the right (between 300 to 345 degrees)
        range_right = msg.ranges[315:345]
        # to the left (between 15 to 60 degrees)
        range_left = msg.ranges[60:55:-1]
        # get the minimum values of each range 
        # minimum value means the shortest obstacle from the robot
        #min_range,i_range = min( (ranges[i_range],i_range) for i_range in range(len(ranges)) )
        min_front,i_front = min( (range_front[i_front],i_front) for i_front in range(len(range_front)) )
        min_right,i_right = min( (range_right[i_right],i_right) for i_right in range(len(range_right)) )
        min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in range(len(range_left )) )


    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        self.sum_speed += lin_vel
        self.update_num += 1.0
        self.avg_speed += self.sum_speed/self.update_num

        self.cmd_vel_pub.publish(cmd_vel)

    #def callback(command, update_num, sum_speed, avg_speed):
        #update_num += 1.0
        #sum_speed += command.linear.x
        #avg_speed += sum_speed/update_num
        #rospy.loginfo(('current_lin_vel: %f'%(command.linear.x), 'lin_avg: %f'%(avg_speed), 'pos' : {
        #    'x: %f'%(position.pose.pose.position.x),
        #    'y: %f'%(position.pose.pose.position.y)
        #}))
        #,
        #'lin_avg: [%f]'%


    def _start_(self):
        print("Turning...")
        self.updatecommandVelocity(0.1,-0.5)
        time.sleep(2)
        

    def controlLoop(self):
        global near_wall
        
        rate = rospy.Rate(10)
        print("start of CL")
        while(near_wall == 0 and not rospy.is_shutdown()):
            print("b4 if") #1
            if(min_front > 0.4 and min_right > 0.3 and min_left > 0.3):
                print("b4 updatecommandvelocity")
                self.updatecommandVelocity(0.15,-0.1)    
                print ("C")
            elif(min_left < 0.2):           # if wall on left, start tracking
                near_wall = 1       
                print ("A")            
            else:
                self.updatecommandVelocity(0.0, -0.25) # if not on left, turn right


        else:   # left wall detected
            if(min_front > 0.3): #2
                if(min_left < 0.22):    #3
                    print("Range: {:.2f}m - Too close. Backing up.".format(min_left))
                    self.updatecommandVelocity(0.0, -0.18)

                elif(0.42 > min_left > 0.25):  #4
                    print("Range: {:.2f}m - Wall-following; turn left.".format(min_left))
                    self.updatecommandVelocity(0.1, 0.15)

                elif(1.0 > min_left > 0.42):
                    print("Range: {:.2f}m - CRANK IT UUUUUUUUUUUUUUUUP STEPBROTHER; turn left.".format(min_left))
                    self.updatecommandVelocity(0.15, min_left * 1.5)

                elif(min_left > 1.0):
                    print("Range: {:.2f}m - CRANK IT UUUUUUUUUUUUUUUUP STEPBROTHER; turn left.".format(min_left))
                    self.updatecommandVelocity(0.2, min_left * 0.6)
                        
                else:
                    print("Range: {:.2f}m - Wall-following; turn right.".format(min_left))
                    self.updatecommandVelocity(0.15, -0.1)
                        
                        
            else:   #5
                print("Front obstacle detected. Turning away.")
                self.updatecommandVelocity(0.0, -0.35)

                while(min_front < 0.3 and not rospy.is_shutdown()):  
                    pass
                rate.sleep()

#Main program
if __name__ == "__main__":
    rospy.init_node("turtlebot3_pydrive")
    bot = Robot()

    while not rospy.is_shutdown():
        try:
            #bot._start_()
            print("b4 controlloop")
            bot.controlLoop()
            print("after controlloop")

            data = {
                #'current_lin_vel': bot.lin_vel,
                'lin_avg': bot.avg_speed,
                'collisions': bot.collisions,
                'pos' : {
                    'x': bot.current_position[0],
                    'y': bot.current_position[1]
                }
            }
            rospy.loginfo(data)
        
        except:
            exit()
        

