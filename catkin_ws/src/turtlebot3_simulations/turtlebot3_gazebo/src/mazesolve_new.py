#Libraries
from __future__ import print_function
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy, time
import numpy as np

# We have translated drive.cpp and drive.h file to python, and modified it to fit our vision



CENTER          = 0
LEFT_NINETY     = 1
RIGHT_NINETY    = 2

GET_DIRECTION               = 0
JEEVES_DRIVE_FORWARD        = 1
JEEVES_U_TURN               = 2
JEEVES_LEFT_NINETY_TURN     = 3
JEEVES_RIGHT_NINETY_TURN    = 4

LINEAR_VELOCITY         = 0.5
ANGULAR_VELOCITY        = 0.6

def scan_callback(msg):
    global range_front
    global range_right
    global range_left
    global ranges
    global min_front,i_front, min_right,i_right, min_left ,i_left
    
    
    # Scan  for differented ranges of angles at the fron right and left
    ranges = msg.ranges
    # in front of the robot (between 5 to -5 degrees)
    range_front[:5] = msg.ranges[5:0:-1]  
    range_front[5:] = msg.ranges[-1:-5:-1]
    # to the right (between 300 to 345 degrees)
    range_right = msg.ranges[315:345]
    # to the left (between 15 to 60 degrees)
    range_left = msg.ranges[60:55:-1]
    # get the minimum values of each range 
    # minimum value means the shortest obstacle from the robot
    #min_range,i_range = min( (ranges[i_range],i_range) for i_range in range(len(ranges)) )
    min_front,i_front = min( (range_front[i_front],i_front) for i_front in range(len(range_front)) )
    min_right,i_right = min( (range_right[i_right],i_right) for i_right in range(len(range_right)) )
    min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in range(len(range_left )) )

range_front = []
range_right = []
range_left  = []
min_front = 0
i_front = 0
min_right = 0
i_right = 0
min_left = 0
i_left = 0

check_forward_dist_ = 0.0
check_side_dist_ = 0.0

near_wall = 0 # start with 0, when we get to a wall, change to 1

class Jeeves():
    # Used to decide direction.

    cmd_vel_pub_:rospy.Publisher = None
    laser_scan_sub_:rospy.Subscriber = None
    odom_sub_:rospy.Subscriber = None

    speed_log_update = 0.0
    speed_log_sum = 0.0
    speed_log_average = 0.0

    def __init__(self):

        # Initialize all variables
        self.range_front = []
        self.range_right = []
        self.range_left  = []

        self.ranges = 0.0
        self.min_front = 0.0
        self.i_front = 0.0
        self.min_right = 0.0
        self.i_right = 0.0
        self.min_left = 0.0
        self.i_left = 0.0

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, scan_callback, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)

    #def __del__(self):
        #self.updatecommandVelocity(0.0, 0.0)
        #rospy.signal_shutdown()
    
    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)
            
        self.jeeves_pose_ = np.arctan2(siny, cosy)

    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        self.speed_log_sum += lin_vel
        self.speed_log_update += 1.0
        self.speed_log_average += self.speed_log_sum / self.speed_log_update

        self.cmd_vel_pub_.publish(cmd_vel)


    def _start_(self):
        print("Turning...")
        self.updatecommandVelocity(0.1,-0.5)
        time.sleep(2)
        

    def controlLoop(self):
        rate = rospy.Rate(10)
        # The algorithm:
        # 1. Robot moves forward to be close to a wall
        # 2. Start following left wall.
        # 3. If too close to the left wall, reverse a bit to get away
        # 4. Otherwise, follow wall by zig-zagging along the wall
        # 5. If front is close to a wall, turn until clear
        while(near_wall == 0 and not rospy.is_shutdown()): #1
            if(self.min_front > 0.4 and self.min_right > 0.3 and self.min_left > 0.3):
                self.updatecommandVelocity(0.15,-0.1)    
                print ("C")
            elif(self.min_left < 0.2):           # if wall on left, start tracking
                near_wall = 1       
                print ("A")            
            else:
                self.updatecommandVelocity(0.0, -0.25) # if not on left, turn right


        else:   # left wall detected
            if(self.min_front > 0.3): #2
                if(self.min_left < 0.22):    #3
                    print("Range: {:.2f}m - Too close. Backing up.".format(self.min_left))
                    self.updatecommandVelocity(0.0, -0.18)

                elif(0.42 > self.min_left > 0.25):  #4
                    print("Range: {:.2f}m - Wall-following; turn left.".format(self.min_left))
                    self.updatecommandVelocity(0.1, 0.15)

                elif(1.0 > self.min_left > 0.42):
                    print("Range: {:.2f}m - CRANK IT UUUUUUUUUUUUUUUUP STEPBROTHER; turn left.".format(self.min_left))
                    self.updatecommandVelocity(0.15, self.min_left * 1.5)

                elif(self.min_left > 1.0):
                    print("Range: {:.2f}m - CRANK IT UUUUUUUUUUUUUUUUP STEPBROTHER; turn left.".format(self.min_left))
                    self.updatecommandVelocity(0.2, self.min_left * 0.6)
                        
                else:
                    print("Range: {:.2f}m - Wall-following; turn right.".format(self.min_left))
                    self.updatecommandVelocity(0.15, -0.1)
                        
                        
            else:   #5
                print("Front obstacle detected. Turning away.")
                self.updatecommandVelocity(0.0, -0.35)

                while(self.min_front < 0.3 and not rospy.is_shutdown()):  
                    pass
                rate.sleep()

#Main program
if __name__ == "__main__":
    rospy.init_node("turtlebot3_pydrive")
    bot = Jeeves()

    while not rospy.is_shutdown():
        try:
            bot._start_()
            bot.controlLoop()
        
        except:
            exit()
        

