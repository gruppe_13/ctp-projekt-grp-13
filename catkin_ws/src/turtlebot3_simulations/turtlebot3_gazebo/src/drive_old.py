#Libraries
from __future__ import print_function
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
import rospy
import numpy as np

# We have translated drive.cpp and drive.h file to python, and modified it to fit our vision
CENTER  = 0
LEFT    = 1
RIGHT   = 2

GET_DIRECTION    = 0
JEEVES_DRIVE_FORWARD    = 1
JEEVES_RIGHT_TURN       = 2
JEEVES_LEFT_TURN        = 3

# Turtlebot Class
class Jeeves():

# Used to decide direction.

    cmd_vel_pub_:rospy.Publisher = None
    laser_scan_sub_:rospy.Subscriber = None
    odom_sub_:rospy.Subscriber = None

    jeeves_state_num = 0.0
    escape_range_ = 0.0

    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0

    scan_data_ = [0.0, 0.0, 0.0]

    jeeves_pose_ = 0.0
    prev_jeeves_pose_ = 0.0

    speed_log_update = 0.0
    speed_log_sum = 0.0
    speed_log_average = 0.0

    def __init__(self):

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        # initialize variables
        self.escape_range_ = 30.0 * (np.pi / 180.0)
        self.check_forward_dist_ = 0.7
        self.check_side_dist_ = 0.6

        self.jeeves_pose_ = 0.0
        self.prev_jeeves_pose_ = 0.0

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallBack, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)
    
    def __del__(self):
         self.updatecommandVelocity(0.0, 0.0)
         rospy.signal_shutdown()def __init__(self):
    
    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)
        
        self.jeeves_pose_ = np.arctan2(siny, cosy)

    def laserScanMsgCallBack(self, data:LaserScan):
        scan_angle = [0, 30, 330]

    # Orientation
        for i in range(len(scan_angle)):
            if np.isinf(data.ranges[i]):
                self.scan_data_[i] = data.range_max
            else:
                self.scan_data_[i] = data.ranges[i]

    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        self.speed_log_sum += lin_vel
        self.speed_log_update += 1.0
        self.speed_log_average += speed_log_sum / speed_log_update

        rospy.loginfo(cmd_vel)
        self.cmd_vel_pub_.publish(cmd_vel)

    def path_detection(self):


        return 0

    def controlLoop(self):
        rate = rospy.Rate(100) # 100Hz
        self.updatecommandVelocity(1.0, 0.0)

        #Orientation
        if self.jeeves_state_num == GET_DIRECTION:

            if self.scan_data_[CENTER] > self.check_forward_dist_:
                if self.scan_data_[LEFT] < self.check_forward_dist_:
                    self.prev_jeeves_pose_ = self.jeeves_pose_
                    self.jeeves_state_num = JEEVES_RIGHT_TURN

                elif self.scan_data_[RIGHT] < self.check_side_dist_:
                    self.prev_jeeves_pose_ = self.jeeves_state_num
                    self.jeeves_state_num = self.JEEVES_LEFT_TURN

                else:
                    jeeves_state_num = JEEVES_DRIVE_FORWARD
        
            if self.scan_data_[CENTER] < self.check_forward_dist_:
                prev_jeeves_pose_ = jeeves_pose_
                jeeves_state_num = JEEVES_RIGHT_TURN

            #U-turn
            if abs(self.prev_jeeves_pose_ - self.jeeves_pose_) >= 2.6:
                jeeves_state_num = GET_DIRECTION
            else:
                updatecommandVelocity(0.0, -1.0)


        # Drive forward        
        elif self.jeeves_state_num == JEEVES_DRIVE_FORWARD:
            self.updatecommandVelocity(0.5,0.0)
            self.jeeves_state_num = GET_DIRECTION
            
        # Right Turn
        elif self.jeeves_state_num == JEEVES_RIGHT_TURN:
            self.updatecommandVelocity(0.0,-0.5)
            self.jeeves_state_num = GET_DIRECTION

        # Left Turn
        elif self.jeeves_state_num == JEEVES_LEFT_TURN:
            self.updatecommandVelocity(0.0,0.5)
            self.jeeves_state_num = GET_DIRECTION

        else:
            self.jeeves_state_num = GET_DIRECTION
        rate.sleep()


#Main program
if __name__ == "__main__":
    rospy.init_node("turtlebot3_pydrive")

    bot = Jeeves()

    while not rospy.is_shutdown():
        bot.controlLoop()
        rospy.spin()