import rospy, time
import numpy as np
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

def scan_callback(msg):
    global range_front
    global range_right
    global range_left
    global ranges
    global min_front,i_front, min_right,i_right, min_left ,i_left
    
    
    # Scan  for differented ranges of angles at the fron right and left
    ranges = msg.ranges
    # in front of the robot (between 5 to -5 degrees)
    range_front[:5] = msg.ranges[5:0:-1]  
    range_front[5:] = msg.ranges[-1:-5:-1]
    # to the right (between 300 to 345 degrees)
    range_right = msg.ranges[315:345]
    # to the left (between 15 to 60 degrees)
    range_left = msg.ranges[60:55:-1]
    # get the minimum values of each range 
    # minimum value means the shortest obstacle from the robot
    #min_range,i_range = min( (ranges[i_range],i_range) for i_range in range(len(ranges)) )
    min_front,i_front = min( (range_front[i_front],i_front) for i_front in range(len(range_front)) )
    min_right,i_right = min( (range_right[i_right],i_right) for i_right in range(len(range_right)) )
    min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in range(len(range_left )) )
    

# Initialize all variables
range_front = []
range_right = []
range_left  = []
min_front = 0
i_front = 0
min_right = 0
i_right = 0
min_left = 0
i_left = 0

check_forward_dist_ = 0.0
check_side_dist_ = 0.0

lin_vel = 0.0
collisions = 0.0
update_num = 0.0
avg_speed = 0.0
sum_speed = 0.0
current_pose = 0.0

# Create the node
cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size = 1) # to move the robot
scan_sub = rospy.Subscriber('scan', LaserScan, scan_callback)   # to read the laser scanner
rospy.init_node('maze_explorer')

command = Twist()
command.linear.x = 0.0
command.angular.z = 0.0
        
rate = rospy.Rate(10)
time.sleep(1) # wait for node to initialize

position = Odometry()
current_position = (position.pose.pose.position.x, position.pose.pose.position.y)

def odomMessageCallback(data:Odometry):
            siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
            cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)
            
            position = np.arctan2(siny, cosy)

def current_lin_vel(command):
        lin_vel = command.linear.x

def average_speed(command, update_num, sum_speed, avg_speed):
    update_num += 1.0
    sum_speed += command.linear.x
    avg_speed += sum_speed/update_num


#def callback(command, update_num, sum_speed, avg_speed):
    #update_num += 1.0
    #sum_speed += command.linear.x
    #avg_speed += sum_speed/update_num
    #rospy.loginfo(('current_lin_vel: %f'%(command.linear.x), 'lin_avg: %f'%(avg_speed), 'pos' : {
    #    'x: %f'%(position.pose.pose.position.x),
    #    'y: %f'%(position.pose.pose.position.y)
    #}))
    #,
    #'lin_avg: [%f]'%
def current_lin_vel(command):
    lin_vel = command.linear.x

print_info = {
    'lin_avg': avg_speed,
    'collisions': collisions,
    'pos' : {
        'x': position.pose.pose.position.x,
        'y': position.pose.pose.position.y
    }
}

near_wall = 0 # start with 0, when we get to a wall, change to 1

# Turn the robot right at the start
# to avoid the 'looping wall'
print("Turning...")
command.angular.z = -0.5
command.linear.x = 0.1
cmd_vel_pub.publish(command)
time.sleep(2)
       
while not rospy.is_shutdown():
    rospy.loginfo(print_info)
    #callback(command, update_num, sum_speed, avg_speed)
    # The algorithm:
    # 1. Robot moves forward to be close to a wall
    # 2. Start following left wall.
    # 3. If too close to the left wall, reverse a bit to get away
    # 4. Otherwise, follow wall by zig-zagging along the wall
    # 5. If front is close to a wall, turn until clear
    while(near_wall == 0 and not rospy.is_shutdown()): #1
        print("Moving towards a wall.")
        if(min_front > 0.4 and min_right > 0.3 and min_left > 0.3):    
            command.angular.z = -0.1    # if nothing near, go forward
            command.linear.x = 0.15
            print ("C")
        elif(min_left < 0.2):           # if wall on left, start tracking
            near_wall = 1       
            print ("A")            
        else:
            command.angular.z = -0.25   # if not on left, turn right 
            command.linear.x = 0.0

        cmd_vel_pub.publish(command)
        
    else:   # left wall detected
        if(min_front > 0.3): #2
            if(min_left < 0.22):    #3
                print("Range: {:.2f}m - Too close. Backing up.".format(min_left))
                command.angular.z = -0.18
                command.linear.x = 0.0
            elif(0.42 > min_left > 0.25):  #4
                print("Range: {:.2f}m - Wall-following; turn left.".format(min_left))
                command.angular.z = 0.1
                command.linear.x = 0.15

            elif(1.0 > min_left > 0.42):
                print("Range: {:.2f}m - CRANK IT UUUUUUUUUUUUUUUUP STEPBROTHER; turn left.".format(min_left))
                command.angular.z = min_left * 1.5
                command.linear.x = 0.15

            elif(min_left > 1.0):
                print("Range: {:.2f}m - CRANK IT UUUUUUUUUUUUUUUUP STEPBROTHER; turn left.".format(min_left))
                command.angular.z = min_left * 0.6
                command.linear.x = 0.2
            else:
                print("Range: {:.2f}m - Wall-following; turn right.".format(min_left))
                command.angular.z = -0.1
                command.linear.x = 0.15
                
        else:   #5
            print("Front obstacle detected. Turning away.")
            command.angular.z = -0.35
            command.linear.x = 0.0
            cmd_vel_pub.publish(command)
            while(min_front < 0.3 and not rospy.is_shutdown()):      
                pass
        # publish command 
        cmd_vel_pub.publish(command)
    # wait for the loop
    rate.sleep()
    