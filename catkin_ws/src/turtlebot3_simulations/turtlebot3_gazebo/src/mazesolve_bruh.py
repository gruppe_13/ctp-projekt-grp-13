from pickle import FALSE, TRUE
import rospy, time
import numpy as np
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

range_front = []
range_right = []
range_left  = []
min_front = 0
i_front = 0
min_right = 0
i_right = 0
min_left = 0
i_left = 0

# The coordinates of our finishing zone to signal robot shutdown
y_end = 3 
x_end = -3

min_min = 1

near_wall = FALSE # Boolean value indicating if nearby wall on left

class Jeeves():                                                                             # Robot Class

    # Initialize all variables

    cmd_vel_pub:rospy.Publisher = None                                                      # Traversing - Publisher
    scan_sub:rospy.Subscriber = None                                                        # Navigation - Subscriber
    odom_sub:rospy.Subscriber = None                                                        # Position - Subscriber 

    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0

    collisions = 0
    update_num = 0.0
    avg_speed = 0.0
    sum_speed = 0.0
    current_position = (0, 0)

    def __init__(self):                                                                     # Constructer
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size = 1)                # Moving robot
        self.scan_sub = rospy.Subscriber('scan', LaserScan, self.scan_callback)             # Reading Laser Scanner (function: scan_callback)
        self.odom_sub = rospy.Subscriber('odom', Odometry, self.position, queue_size = 1)   # Robot Poisition       (function: position)
       

    def position(self, data:Odometry):
        self.current_position = (data.pose.pose.position.x, data.pose.pose.position.y)      # Tracking robot's position

        if(bot.current_position[0] < x_end and bot.current_position[1] > y_end):            # Checking if finishing zone has been reaches
            rospy.loginfo(f"Ave linear velocity: {bot.avg_speed}")
            rospy.loginfo(f"collisions: {bot.collisions}self.updatecommandVelocity(0.0, -0.35)")
            self.updatecommandVelocity(0.0,0.0)
            rospy.logwarn("Robot shutdown")
            rospy.signal_shutdown
            exit()
    
    def collision(self):                                                                    # Collision counter
        self.collisions += 1
        rospy.logwarn("Jeeves has hit the wall")

    def scan_callback(self, msg):                                                                                     
        global range_front
        global range_right 
        global range_left
        global ranges
        global min_min,min_front,i_front, min_right,i_right, min_left ,i_left
                                                                                       
        ranges = msg.ranges                                                                 
        range_front[:5] = msg.ranges[5:0:-1]                                                
        range_front[5:] = msg.ranges[-1:-5:-1]                                              
        range_right = msg.ranges[315:345]                                                   
        range_left = msg.ranges[60:55:-1]                                                   

        min_front,i_front = min( (range_front[i_front],i_front) for i_front in range(len(range_front)) ) 
        min_right,i_right = min( (range_right[i_right],i_right) for i_right in range(len(range_right)) ) 
        min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in range(len(range_left )) ) 

        min_min = min(min_front, min_right, min_left)                                       
        
        if (min_min < 0.1):                                                                 
            self.collision()                                                                            
    

    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        #Variables created to receive data on robot navigation
        self.sum_speed += lin_vel                                                                                                          
        self.update_num += 1.0         
        self.avg_speed = self.avg_speed + (lin_vel - self.avg_speed)/self.update_num
        self.current_lin_vel = lin_vel

        self.cmd_vel_pub.publish(cmd_vel)                                                   # Using our publisher node

    def controlLoop(self):                                                                  # Autonomous robot navigation
        global near_wall
        rate = rospy.Rate(10)
        

        while(near_wall == FALSE and not rospy.is_shutdown()):                              # Looping until a wall found

            if(min_front > 0.4 and min_right > 0.3 and min_left > 0.3):                     # If no wall found, drive forward
                self.updatecommandVelocity(0.15,-0.1)    
                
            elif(min_left < 0.2):                                                           # If wall on left, start tracking
                near_wall = TRUE       
                           
            else:                                                                           # Else if not on left, turn right
                self.updatecommandVelocity(0.0, -0.25)                  

        else:                                                           
            if(min_front > 0.3):                                                            # Looping while wall detected                                            

                if(min_left < 0.22):                                                        # If robot is too close- will turn away from wall                                            
                    print("Range: {:.2f}m".format(min_left))
                    self.updatecommandVelocity(0.0, -0.18)

                elif(0.42 > min_left > 0.25):                                               # Take a soft left turn
                    print("Range: {:.2f}m".format(min_left))
                    self.updatecommandVelocity(0.1, 0.15)

                elif(1.0 > min_left > 0.42):                                                # Take a normal left turn
                    print("Range: {:.2f}m".format(min_left))
                    self.updatecommandVelocity(0.15, min_left * 1.5)
           
                elif(min_left > 1.0):                                                       # Take a sharp left turn
                    print("Range: {:.2f}m".format(min_left))
                    self.updatecommandVelocity(0.2, min_left * 0.6)
                        
                else:                                                                       # If robot is following wall, go straight and slighty right
                    print("Range: {:.2f}m".format(min_left))
                    self.updatecommandVelocity(0.15, -0.1)
                        
                        
            else:                                                                           # Front obstacle detected. Turning away
                self.updatecommandVelocity(0.0, -0.35)

                while(min_front < 0.3 and not rospy.is_shutdown()):  
                    pass
                rate.sleep()


if __name__ == "__main__":                                                                  # Main program
    rospy.init_node("Jeeves")
    bot = Jeeves()                                                                                      

    while not rospy.is_shutdown():
    
        bot.controlLoop()                       

        """
        data = {                                                                            # Logging data
                'current_lin_vel': "%.2f" % bot.current_lin_vel, 
                'lin_avg': "%.4f" % bot.avg_speed,
                'collisions': bot.collisions,
                'pos' : {
                    'x': bot.current_position[0],
                    'y': bot.current_position[1]
                }
            }

        rospy.loginfo(data)
        """ # Used to create graphs of traversel. 
            # Delete the """ to get output (recommend outcommenting the prints above to gain data for graphs).
